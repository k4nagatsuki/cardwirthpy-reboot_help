
CardWirthPy Rebootプレイヤーズガイド
====================================

[CardWirthPy Reboot](https://bitbucket.org/k4nagatsuki/cardwirthpy-reboot)向けのヘルプファイルを作るためのプロジェクトですが、まだ内容はほとんど無く、作りかけもいいところです。現時点では、挫折する可能性が大いにあります。

予定される内容
--------------

`CATALOG.txt`に目次を書きました。おおむねこのような構成にするつもりですが、書いていくうちに変わるかもしれません。

最終的に以下のような内容を並立させる事ができればと思っています。

 * CardWirth初心者でも遊び始められるようなチュートリアル
 * 各画面や機能の詳細な解説
 * 標準添付スキンの説明
 * WSN形式の仕様

ビルド
------

以下のようにしてコンパイルして`CardWirthPy.chm`を生成できますが、ヘルプの内容を見るには各HTMLファイルをInternet Explorerで表示するのがもっとも手っ取り早いです(HTMLヘルプはIEのレンダリングエンジンで表示されるので、IEでなければいけません)。

 1. [Microsoft HTML Help Workshop](https://msdn.microsoft.com/ja-jp/library/windows/desktop/ms669985(v=vs.85).aspx)をインストールします。
 2. [Python 3](https://www.python.org/)をインストールします。
 3. `python build.py`

著作権情報
----------

`copyright.html`に書いてある内容でほぼカバーできていると思います。この文書などのカバーできていない分は、[CC0](http://creativecommons.org/choose/zero/)の下に扱ってください。
