#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import shutil
import subprocess
import sys

hhcs = ("hhc", "C:/Program Files/HTML Help Workshop/hhc.exe", "C:/Program Files (x86)/HTML Help Workshop/hhc.exe")
workdir = "_html_files"
if os.path.isdir(workdir):
    shutil.rmtree(workdir)

target = re.compile("\\A[0-9]{2}_.+\\Z")
title_re = re.compile("\\A(\\s+<title>.+) @ .+(</title>)\\Z")

lines = [
    "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">",
    "<HTML>",
    "<HEAD>",
    "    <!-- Sitemap 1.0 -->",
    "</HEAD>",
    "<BODY>",
    "<OBJECT type=\"text/site properties\">",
    "    <param name=\"ImageType\" value=\"Folder\">",
    "</OBJECT>",
]

keyword_table = {}


def listup(dpath, tab):
    if dpath == ".":
        lines.append(tab + "<UL>")
    else:
        title = os.path.join(dpath, "title.txt")
        if not os.path.isfile(title):
            return
        with open(title, "rb") as f:
            title = f.read()
        title = title.decode("utf-8").rstrip()
        lines.append(tab + "<LI><OBJECT type=\"text/sitemap\">")
        lines.append(tab + "    <param name=\"Name\" value=\"%s\">" % title)
        lines.append(tab + "    <param name=\"ImageNumber\" value=\"1\">")
        lines.append(tab + "</OBJECT>")
        lines.append(tab + "<UL>")

    for fname in sorted(os.listdir(dpath)):
        if fname == "00_table_of_contents.html":
            continue
        fpath = os.path.normpath(os.path.join(dpath, fname))

        if os.path.isdir(fpath) and fname in ("images", "stylesheets"):
            dst = os.path.join(workdir, fpath)
            if not os.path.isdir(os.path.dirname(dst)):
                os.makedirs(os.path.dirname(dst))
            shutil.copytree(fpath, dst)
        elif os.path.isdir(fpath) and target.match(fname):
            listup(fpath, tab + "    ")
        elif fname.lower().endswith(".html") and target.match(fname):
            sjis_file = os.path.join(workdir, fpath)

            with open(fpath, "rb") as f:
                html = f.read()
            html = html.decode("utf-8")
            i1 = html.find("<title>") + len("<title>")
            i2 = html.find(" @ ")
            if i1 == -1:
                raise Exception(fpath + " <title>")
            if i2 == -1:
                raise Exception(fpath + " <title>")
            name = html[i1:i2]
            lines.append(tab + "    <LI><OBJECT type=\"text/sitemap\">")
            lines.append(tab + "        <param name=\"Name\" value=\"%s\">" % name)
            lines.append(tab + "        <param name=\"Local\" value=\"%s\">" % fpath)
            lines.append(tab + "    </OBJECT>")

            sjis_lines = []
            heads = set()
            for line in html.splitlines():
                toc_t = "<li id=\"toc\"><a href=\"00_table_of_contents.html\">目次</a></li>"
                line = line.replace(toc_t, "")
                toc_t = "<li id=\"toc\"><a href=\"../00_table_of_contents.html\">目次</a></li>"
                line = line.replace(toc_t, "")
                if line.strip() == "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">":
                    line2 = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">"
                    sjis_lines.append(line2.encode("cp932"))
                else:
                    matcher = title_re.match(line)
                    if matcher:
                        line2 = "".join(matcher.groups())
                        sjis_lines.append(line2.encode("cp932"))
                    else:
                        sjis_lines.append(line.encode("cp932"))

                i = line.find("<!-- Keywords:")
                j = line.find(" -->", i)
                k = line.find("id=\"")
                l = line.find("\"", k+len("id=\""))
                if i == -1 or j == -1 or k == -1 or l == -1:
                    continue
                keywords = line[i+len("<!-- Keywords:"):j]
                if not keywords:
                    continue
                head = line[k+len("id=\""):l]
                if head in heads:
                    raise Exception("ID DUPLICATED! + " + fpath)

                for keyword in keywords.split(','):
                    kw_seq = keyword_table.get(keyword, [])
                    kw_seq.append((fpath + "#" + head, name))
                    keyword_table[keyword] = kw_seq

            sjis_html = os.linesep.encode("cp932").join(sjis_lines)
            if not os.path.isdir(os.path.dirname(sjis_file)):
                os.makedirs(os.path.dirname(sjis_file))
            with open(sjis_file, "wb") as f:
                f.write(sjis_html)

    lines.append(tab + "</UL>")


listup(".", "")

lines.append("</BODY>")
lines.append("</HTML>")
lines.append("")

kw_lines = [
    "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">",
    "<HTML>",
    "<HEAD>",
    "    <!-- Sitemap 1.0 -->",
    "</HEAD>",
    "<BODY>",
    "<OBJECT type=\"text/site properties\">",
    "    <param name=\"FrameName\" value=\"current\">",
    "</OBJECT>",
    "<UL>",
]

for keyword in sorted(keyword_table.keys()):
    kw_lines.append("    <LI><OBJECT type=\"text/sitemap\">")
    kw_lines.append("        <param name=\"Name\" value=\"%s\">" % keyword)
    for html, title in keyword_table[keyword]:
        if 1 < len(keyword_table):
            kw_lines.append("        <param name=\"Name\" value=\"%s\">" % title)
        kw_lines.append("        <param name=\"Local\" value=\"%s\">" % html)
    kw_lines.append("    </OBJECT></LI>")

kw_lines.append("</UL>")
kw_lines.append("</BODY>")
kw_lines.append("</HTML>")

s = os.linesep.join(lines)
# s = s.decode("utf-8")
s = s.encode("cp932")
with open(os.path.join(workdir, "cardwirthpy-reboot_help.hhc"), "wb") as f:
    f.write(s)

s = os.linesep.join(kw_lines)
# s = s.decode("utf-8")
s = s.encode("cp932")
with open(os.path.join(workdir, "cardwirthpy-reboot_help.hhk"), "wb") as f:
    f.write(s)

shutil.copy("cardwirthpy-reboot_help.hhp", os.path.join(workdir, "cardwirthpy-reboot_help.hhp"))

for hhc in hhcs:
    try:
        subprocess.call("\"%s\" %s" % (hhc, os.path.join(workdir, "cardwirthpy-reboot_help.hhp")))
        break
    except Exception:
        pass
else:
    raise Exception()

shutil.move(os.path.join(workdir, "CardWirthPy.chm"), "CardWirthPy.chm")
shutil.rmtree(workdir)
